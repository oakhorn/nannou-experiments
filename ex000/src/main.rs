use nannou::prelude::*;

fn main() {
    nannou::app(model).simple_window(view).run()
}

struct Model {
    rects: Vec<(f32, Rect)>,
    bg_color: Srgba
}

fn model(app: &App) -> Model {
    // Maximize window
    app.main_window().set_inner_size_pixels(1900, 1200);
    //app.set_loop_mode(LoopMode::NTimes { number_of_updates: 256 });

    let rect_count = 20;
    let rects: Vec<(f32, Rect)> = (0..rect_count)
        .scan(Rect::from_w_h(2000., 2000.), |prev, _b: i32| {
            let rect = Rect::from_w_h(prev.w() * 0.707107, prev.h() * 0.707107);
            *prev = rect;
            Some(rect)
        })
        .enumerate()
        .map(|(i, rect)| {
            let fract = i as f32 / rect_count as f32;
            (fract, rect)
        })
        .collect();
    let bg_color = srgba(1.0, 1.0, 1.0, 1.0);

    Model { rects: rects, bg_color: bg_color}
}

fn view(app: &App, model: &Model, frame: Frame) {
    // Begin drawing
    let draw = app.draw();

    // Clear the background.
    draw.background().color(model.bg_color);

    let _win = app.window_rect();
    let t = app.time;
    let freq = (10. * t / TAU).sin() / 2. + 0.5;
    let g_mod = 0.33;
    let b_mod = 0.66;
    

    // Consider trying with a different shape to figure out what's causing the performance issues
    model.rects
        .iter()
        .for_each(|(fract, rect)| {
            let r = (fract + freq) % 1.0;
            let g = (fract + freq + g_mod) % 1.0;
            let b = (fract + freq + b_mod) % 1.0;
            let rgba = srgba(r, g, b, 0.4);

            draw.rect()
                .xy(rect.xy())
                .wh(rect.wh())
                .z_degrees(t * TAU * 100.0 * fract)
                .color(rgba);
        });

    // Write the result of our drawing to the window's frame.
    draw.to_frame(app, &frame).unwrap();
}