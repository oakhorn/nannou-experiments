use nannou::prelude::*;
use nannou::geom::ellipse::Ellipse;
// use log::{info, error, warn, debug};
// use simple_logger;

fn main() {
    // simple_logger::init().unwrap();
    let builder = nannou::app(model);
    let builder = builder.simple_window(view);
        builder.run();
}

const PERIODICITY: usize = 60;
const MAX_CIRCLES: usize = 100000;
const MAX_RESOLUTION: usize = 4;
const WINDOW_DIMENSIONS: (u32, u32) = (1920, 1200);
const ENVELOP_SIZE_THRESHOLD: f32 = 0.25;
const GEN_SIZE_THRESHOLD: f32 = 1. / 32.;
const RENDER_FRAMES: bool = true;
const CONSTRAIN_LOOP_TO_PERIOD: bool = true;
const LOOP_COUNT: usize = 4;

struct Model {
    circles: Vec<Circle>
}

#[derive(Clone, Copy)]
enum Direction {
    Forward,
    Reverse
}

impl Direction {
    fn opposite(self) -> Direction {
        match self {
            Direction::Forward => Direction::Reverse,
            Direction::Reverse => Direction::Forward
        }
    }
}

#[derive(Clone, Copy)]
struct Circle {
    ellipse: Ellipse,
    offset: Point2,
    direction: Direction,
}

impl Circle {
    fn new(ellipse: Ellipse, x_offset: f32, direction: Direction) -> Circle {
        Circle { ellipse: ellipse, offset: Point2::new(x_offset, 0.), direction: direction }
    }

    fn w_h(self) -> f32 {
        self.ellipse.rect.w()
    }

    fn left(self) -> f32 {
        self.offset.x
    }

    fn right(self) -> f32 {
        self.offset.x + self.w_h()
    }
    
    fn center(self) -> f32 {
        self.left() + (self.right() - self.left()) / 2.
    }

    fn fork(self) -> (Circle, Circle) {
        let l = Rect::from_w_h(self.w_h() * 0.5, self.w_h() * 0.5);
        let r = Rect::from_w_h(self.w_h() * 0.5, self.w_h() * 0.5);

        (
            Circle::new(
                Ellipse::new(l, self.ellipse.resolution / 2),
                self.left() - self.w_h() * 0.25,
                self.direction.opposite()
            ),
            Circle::new(
                Ellipse::new(r, self.ellipse.resolution / 2),
                self.right() - self.w_h() * 0.25,
                self.direction
            ),
        )
    }

    fn envelop(self, count: usize) -> Vec<Circle> {
        let mut circles: Vec<Circle> = vec![];
        circles.push(self);

        let mut next_add = 0.125;
        let mut multiplier = 0.75;
        let mut current_size = self.w_h() * 0.75;
        let mut last_direction = self.direction.opposite();

        for _i in 0..count {
            if current_size < ENVELOP_SIZE_THRESHOLD {
                break;
            } 

            circles.push(
                Circle::new(
                    Ellipse::new(
                        Rect::from_w_h(current_size, current_size),
                        2
                    ),
                    self.offset.x - (self.w_h() - current_size) * 0.5,
                    last_direction
                )
            );
            current_size = self.w_h() * (multiplier + next_add);
            multiplier += next_add;
            next_add *= 0.5;
            last_direction = last_direction.opposite();
        }

        circles.reverse();
        circles
    }
}

fn model(app: &App) -> Model {
    let (window_w, window_h) = WINDOW_DIMENSIONS;
    app.main_window().set_inner_size_pixels(window_w, window_h);
    // app.main_window().set_maximized(true);
    
    if CONSTRAIN_LOOP_TO_PERIOD {
        app.set_loop_mode(LoopMode::NTimes { number_of_updates: PERIODICITY * LOOP_COUNT });
    }

    let circles = gen_circles(window_w as f32 * 4., window_w as f32 / -2.);

    Model { circles: circles }
}

fn gen_circles(max_radius: f32, x_offset: f32) -> Vec<Circle> {
    let mut circles: Vec<Circle> = vec![];
    let initial_circle = Circle::new(
        Ellipse::new(
            Rect::from_w_h(max_radius, max_radius),
            MAX_RESOLUTION
        ),
        x_offset,
        Direction::Reverse
    );
    circles.push(initial_circle);

    let mut next_fork = 0;

    while circles.len() < MAX_CIRCLES - 2 {
        let (l, r) = circles.as_slice()[next_fork].fork();

        if l.w_h() < GEN_SIZE_THRESHOLD {
            break;
        }

        circles.push(l);
        circles.push(r);

        next_fork += 1;
    }

    circles.iter()
        .flat_map(|c| {
            c.envelop(9)
        })
        .collect()
}

fn hue(direction: Direction, delta: f32, width: f32, offset: f32) -> f32 {
    match direction {
        Direction::Forward => {
            ((delta  + width + offset) / 3.) % 1.0
        }
        Direction::Reverse => {
            1.0 - ((delta  + width + offset) / 3.) % 1.0
        }
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    // Begin drawing
    let draw = app.draw();

    // Clear the background.
    draw.background().color(srgba(0., 0., 0., 1.));

    let window = app.window_rect();
    let _t = app.time;
    let f = app.elapsed_frames() as f32;
    let fraction = (f - 1.) / PERIODICITY as f32;
    let exp_fract = 2.0.pow(fraction + 1.) as f32;
    let max_fract = 2.0.pow(LOOP_COUNT as f32) as f32;
    let half_w = window.w() * 0.5;

    model.circles.iter()
        .for_each(|c| {
            if c.left() >= window.left() && c.right() <= window.right() || c.center() >= window.left() && c.center() <= window.right() || c.w_h() >= window.w() {
                let w_h = c.w_h() * exp_fract;
                let w_h_normal = c.w_h() * max_fract / w_h;
                // let offset = (c.offset.x + half_w) * exp_fract - half_w;
                let offset = (c.offset.x + half_w) * exp_fract - half_w + window.w() * (1. - exp_fract);
                let offset_normal = (c.offset.x + half_w) * max_fract / ((c.offset.x + half_w) * exp_fract); 
                let hue = hue(c.direction, fraction, w_h_normal, offset_normal);
                draw.ellipse()
                    .hsva(hue, 1.0, 0.5, 0.33 + 0.01 * exp_fract)
                    .w_h(w_h, w_h)
                    .x_y(offset, 0.);
            }
        });

    // Write the result of our drawing to the window's frame.
        draw.to_frame(app, &frame).unwrap();

    if RENDER_FRAMES {
        app.main_window().capture_frame(format!("out/{:03}.png", frame.nth()));
    }
}